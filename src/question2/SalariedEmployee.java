package question2;

public class SalariedEmployee implements Employee {
	
	private double yearlyPay;
	
	public SalariedEmployee(double yearlyPay) {
		this.yearlyPay = yearlyPay;
	}
	
	//get the private field yearlyPay
	public double getYearlyPay() {
		return this.yearlyPay;
	}
	
	//calculate the weekly salary
	public double getWeeklyPay() {
		double salary = this.yearlyPay / 52;
		return salary;
	}
}
