package question2;

public class HourlyEmployee implements Employee{
	
	private double hour;
	private double hourlyPay;
	
	public HourlyEmployee(double hour, double hourlyPay) {
		this.hour = hour;
		this.hourlyPay = hourlyPay;
	}
	
	//get the private field hour
	public double getHour() {
		return this.hour;
	}
	
	
	//get the private field hourlyPay
	public double getHourlyPay() {
		return this.hourlyPay;
	}
	
	
	//calculate the weekly salary
	public double getWeeklyPay() {
		double salary =  this.hour * this.hourlyPay;
		return salary;
	}
}
