package question2;

public class PayrollManagement {
	
	//initialize an Employee array with hard code contents and call the getTotalExpenses method using the array as the input
	public static void main(String[] args) {
		Employee[] ouremployees = new Employee[5]; 
		ouremployees[0] = new SalariedEmployee(104000);
		ouremployees[1] = new HourlyEmployee(40, 25);
		ouremployees[2] = new UnionizedHourlyEmployee(60, 30, 40, 2);
		ouremployees[3] = new SalariedEmployee(200001);
		ouremployees[4] = new HourlyEmployee(30, 50);
		getTotalExpenses(ouremployees);
	}
	
	//calculate the total amount of expense per week by getting the sum of weekly salary of all employees in the input array
	public static double getTotalExpenses(Employee[] input) {
		double total = 0;
		for(int i = 0; i < input.length; i++) {
			total += input[i].getWeeklyPay();
		}
		System.out.println(total);
		return total;
	}
}
