package question2;

public class UnionizedHourlyEmployee extends HourlyEmployee {
	
	private double maxHoursPerWeek;
	private double overtimeRate;
	
	public UnionizedHourlyEmployee(double hour, double hourlyPay, double maxHoursPerWeek, double overtimeRate) {
		super(hour, hourlyPay);
		this.maxHoursPerWeek = maxHoursPerWeek;
		this.overtimeRate =  overtimeRate;
	}
	
	//get private field maxHoursPerWeek
	public double getMaxHoursPerWeek() {
		return this.maxHoursPerWeek;
	}
	
	//get private field overtimeRate
	public double getOvertimeRate() {
		return this.overtimeRate;
	}
	
	//calculate the weekly salary
	public double getWeeklyPay() {
		double salary;
		if(this.maxHoursPerWeek >= super.getHour()) {
			salary = super.getHourlyPay() * super.getHour();
		}
		else {
			salary = super.getHourlyPay() * super.getHour() + super.getHourlyPay() * this.overtimeRate * (super.getHour() - this.maxHoursPerWeek);
		}
		return salary;
	}
}
