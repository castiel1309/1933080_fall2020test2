package question3;

import java.util.*;

public class CollectionMethods {
	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size){
		Collection<Planet> larger = new ArrayList<Planet>();
		for (Planet p : planets) {
			if(p.getRadius() >= size) {
				larger.add(p);
			}
		}
		return larger;
	}
			
}
